#include "notepad.h"
#include "ui_notepad.h"
#include <QFile>
#include<QFileDialog>
#include<QMessageBox>
#include<QPrinter>
#include<QTextStream>
#include<QPrintDialog>
#include<QPainter>

Notepad::Notepad(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Notepad)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->textEdit);
}

Notepad::~Notepad()
{
    delete ui;
}

void Notepad::on_actionNew_triggered()
{
    if(currentFile.isNull() || currentFile.isEmpty())
    {
        currentFile="";
        ui->textEdit->setText("");
    }
    else{
    QMessageBox fileMsgBox;
    fileMsgBox.setText("Do you want to save your changes?");
    fileMsgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
    fileMsgBox.setDefaultButton(QMessageBox::Save);
    int ret = fileMsgBox.exec();

        switch(ret)
        {
            case QMessageBox::Save :
                    on_actionSave_as_triggered();
                    break;
            case QMessageBox::Discard :
                    currentFile = "";
                    ui->textEdit->setText("");
                    break;
        }
    }
}

void Notepad::on_actionOpen_triggered()
{
        bool success=true;

        QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
        QFile file(fileName);
        currentFile = fileName;
        if (!file.open(QIODevice::ReadOnly | QFile::Text)) {
            success=false;
            return;
        }
        QTextStream in(&file);
        QString text = in.readAll();
        ui->textEdit->setText(text);
        file.close();

        if(success==true){
            currentFile=fileName;
        }
}

void Notepad::on_actionSave_triggered()
{
       if(currentFile.isEmpty())
           on_actionSave_as_triggered();
       else
       {
           QFile file(currentFile);
           QTextStream out(&file);
           QString text = ui->textEdit->toPlainText();
           out << text;
           file.flush();
           file.close();
       }
}

void Notepad::on_actionPrint_triggered()
{
    QPrinter printer;

    QPrintDialog *dialog = new QPrintDialog(&printer);
    dialog->setWindowTitle("Print Document");

    if (dialog->exec() != QDialog::Accepted)
           return;

       QPainter painter;
       painter.begin(&printer);

       painter.drawText(100, 100, 500, 500, Qt::AlignLeft|Qt::AlignTop, currentFile);

       painter.end();
       delete dialog;
}

void Notepad::on_actionSave_as_triggered()
{

        QString fileName = QFileDialog::getSaveFileName(this, "Save as");
        QFile file(fileName);
        currentFile = fileName;

        QTextStream out(&file);
        QString text = ui->textEdit->toPlainText();
        out << text;
        file.flush();
        file.close();
}
